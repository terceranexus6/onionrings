# Onionrings

A tool based on torsocks for grabbing information from ransomware leak sites. It works through modules for making it convenient to the community.

![](https://media0.giphy.com/media/9Dk4Om70ianIjMToiz/giphy.gif?cid=790b761184fbb27710839f655761e19d0f06957ad17f1ba8&rid=giphy.gif&ct=g)

## Getting started

**ATTENTION**: Before using an actual leak site, [make sure](https://gitlab.com/terceranexus6/onionrings/-/wikis/Installing-and-checking-torsocks) you have installed tor correctly and that your IP is being conceiled. Either way it could be dangerous.

The tool is meant to be used in a Linux terminal, since it uses bash and python. The design is meant to get bigger with time and a little help. For learning more, check [Adding Modules](https://gitlab.com/terceranexus6/onionrings/-/wikis/Adding-modules) in the Wiki.

Right now, these are the options of the tool:

Asking for help:
```
python3 onionring.py --help
python3 onionring.py -h
```
Listing the available modules:
```
python3 onionring.py --List 
python3 onionring.py -l
```
Deploying a module
```
python3 onionring.py --ransomware <RANSOMWARE>
python3 onionring.py -r <RANSOMWARE>
```

Right now there's only available a testing module which is not a true ransomware leak site, but a Rick Roll site. Execute `python3 onionring.py -r paula` to see it working.

This is how it looks using a module for Conti leak site (downloading and extracting a list of victims in json), the module is still not published, but working:
![](https://gitlab.com/terceranexus6/onionrings/-/raw/main/onionring/images/example.jpeg)

_Note: the pixel process has been made for privacy and precaution. Also, in different level and layers in order to protect it against pixel reversing._

## Collabs
todo


