#!/bin/bash

#colors
RED='\033[0;31m'
PINK='\033[1;35m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color

# torlinks (real ones)

#testing webs
#rickroll='https://www.tomorrowtides.com/onionringexample.html'
paula='https://terceranexus6.gitlab.io/website/'

#tags (testing)
#tag1="rickroll"
tag1="paula"

# This is the list of sites in one array
#declare -a Onions=($rickroll)
declare -a Onions=($paula)

# This are the tags for comparison with the parameter. IMPORTANT: The order shoulw be the same as in Onions.
# Work with it as if it was a Matrix of data.
declare -a tags=($tag1)


len=${#tags[@]}

# In this part, it compares the parameter $1 with the tags, and when (if) there's a match, 
# it takes the same iteration to use torsocks with the link, and download it with wget.

for (( i=0; i<${len}; ++i )); do
	if [ ${tags[$i]} == $1 ] ;then
		mkdir ${tags[$i]}
		cd ${tags[$i]}
		echo -e "${PINK}Downloading content from ${tags[$i]}...${NC}"
		
		# simple wget. TODO: Add other options here of wget, depending on the case
		#torsocks wget "${Onions[$i]}"
		wget "${Onions[$i]}"
	fi
done

echo -e "${GREEN}We are done here!${NC}"
cd ..
